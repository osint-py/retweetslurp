# reTweetSlurp, gulp up the reTweets

The goal is to download retweets of a specific tweet, to analyse them.
Keywords are: 'Python', 'Data Science', 'archive tweets'.

## Option 1 - Twitter API v1
You can download retweets via the (official) Twitter API Version 1 (V1), 
[GET statuses/retweeters/ids](https://developer.twitter.com/en/docs/twitter-api/v1/tweets/post-and-engage/api-reference/get-statuses-retweeters-ids)

The limitations are:
- The number of retweets returned is limited to 100 (unless you pay for a premium developer account).

## Option 2 - Twitter API v2

In V2 [/tweets/retweets/](https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/introduction) there is a new function that will deliver all the retweets in batches of 100 (max).

1. https://github.com/twitterdev/Twitter-API-v2-sample-code/blob/main/Retweets-Lookup/retweeted_by.py
1. https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/introduction
1. https://docs.tweepy.org/en/stable/api.html
1. https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/quick-start/retweets-lookup
1. https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/migrate/retweets-lookup-standard-to-twitter-api-v2

## Option 3 - Twitter API via search

1. v1 Twitter API search [Rules and filtering: Standard v1.1](https://developer.twitter.com/en/docs/twitter-api/v1/rules-and-filtering/search-operators)


## Option 4 - via academicTwitterR
1. https://github.com/cjbarrie/academictwitteR
1. https://rdrr.io/cran/academictwitteR/src/R/get_retweeted_by.R


## Sources
1. https://arxiv.org/pdf/2103.10754.pdf
1. https://freshvanroot.com/blog/2019/twitter-search-guide-by-luca/
1.http://thesocialchic.com/2013/04/26/how-to-master-twitter-search/
1. https://developer.twitter.com/en/docs
1. https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/migrate/retweets-lookup-standard-to-twitter-api-v2
1. https://developer.twitter.com/en/portal/dashboard
1. https://developer.twitter.com/en/docs/twitter-api/tweets/search/api-reference/get-tweets-search-all
1. https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/api-reference/post-users-id-retweets
1. https://developer.twitter.com/en/docs/api-reference-index
1. https://developer.twitter.com/en/docs/twitter-api/v1/rules-and-filtering/search-operators


### Scraping tools
1. https://dripify.io/top-social-media-scraping-tools/
1. https://drkblake.com/tweepy-tweet-scraper-2-o/
1. https://predictivehacks.com/how-to-get-twitter-data-using-python/
1. https://stackoverflow.com/questions/27941940/how-to-exclude-retweets-and-replies-in-a-search-api

# License
CC BY-SA 4.0

